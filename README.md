# README #

Orchard CMS module to dynamically generate XML and HTML sitemaps.

### How do I get set up? ###

* Add to your Orchard configuration and enable, and it should work out of the box.  It'll create two new routes:  /sitemap and /sitemap.xml

### Extensibility ###

* You may create custom implementations of ISitemapProvider to add items to the sitemap.
* You can override Sitemap.cshtml in a client theme for custom styling.

### Who do I talk to? ###

* Robert Misiak <robert@onestop.com>
