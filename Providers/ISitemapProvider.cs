﻿using Onestop.Sitemap.Models;
using Orchard;
using System.Collections.Generic;

namespace Onestop.Sitemap.Providers {
    public interface ISitemapProvider : IDependency {
        IEnumerable<SitemapEntry> GetSitemapEntries();

        /// <summary>
        /// Priority of thie sitemap provider.  Higher number is a higher priority.
        /// </summary>
        int Priority { get; }
    }
}