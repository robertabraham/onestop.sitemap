﻿using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Onestop.Sitemap.Routes {
    [OrchardFeature("Onestop.Sitemap")]
    public class SitemapRoutes : IRouteProvider {
        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Route = new Route("sitemap.xml",
                                      new RouteValueDictionary {
                                          {"area", "Onestop.Sitemap"},
                                          {"controller", "Sitemap"},
                                          {"action", "Xml"}
                                      },
                                      new RouteValueDictionary(),
                                      new RouteValueDictionary {
                                          {"area", "Onestop.Sitemap"}
                                      },
                                      new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route("sitemap",
                                      new RouteValueDictionary {
                                          {"area", "Onestop.Sitemap"},
                                          {"controller", "Sitemap"},
                                          {"action", "Html"}
                                      },
                                      new RouteValueDictionary(),
                                      new RouteValueDictionary {
                                          {"area", "Onestop.Sitemap"}
                                      },
                                      new MvcRouteHandler())
                }
            };
        }

        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }
    }
}