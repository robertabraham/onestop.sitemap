﻿Name: Onestop.Sitemap
AntiForgery: enabled
Author: Robert Misiak, Onestop Internet
Website: http://www.onestop.com
Version: 1.0
OrchardVersion: 1.0
Description: Enable XML and HTML sitemaps
